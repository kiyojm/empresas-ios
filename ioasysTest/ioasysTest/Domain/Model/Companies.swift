//
//  Companies.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
struct Companies: Codable {
    var enterprises : [Company] = []

    
}

struct Company: Codable {
    var enterprise_name: String = ""
    var description: String = ""

}

extension Companies{
    public static func getFromData(json: Data) -> Companies{
        let jsonData = json
        if let companies = try? JSONDecoder().decode(Companies.self, from: jsonData ){
            return companies
        }else{
            return Companies()
        }
    }
}
