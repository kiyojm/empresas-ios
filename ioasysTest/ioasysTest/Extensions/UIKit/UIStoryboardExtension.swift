//
//  UIStoryboardExtension.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 10/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    // MARK: COMPANIES storyboard
    static func buildCompaniesViewController() -> UIViewController {
        return companiesStoryboard.instantiateViewController(withIdentifier: "companies")
    }
    
    // MARK: AUTH storyboard
    static func buildLoginViewController() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "login")
    }
    
    
}

extension UIStoryboard {
    
    static var companiesStoryboard : UIStoryboard {
        return getStoryboard(name: "Companies")
    }
    
    static var loginStoryboard : UIStoryboard {
        return getStoryboard(name: "Login")
    }
    
    
}
extension UIStoryboard {
    
    static func getStoryboard(name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: nil)
    }
}
