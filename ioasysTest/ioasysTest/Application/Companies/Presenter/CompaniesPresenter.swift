//
//  CompaniesPresenter.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
class CompaniesPresenter: BasePresenter {
    
    weak private var view : CompaniesView?
    
    var service: AlamofireService = RestClient()
    
    func attach(view: NSObjectProtocol){
        self.view = view as? CompaniesView
    }
    
    func detach() {
        view = nil
    }
    
    
    func getCompanies() {
        let url = SessionManager.COMPANIES
        self.service.GET(url: url) { (data, success) in
            if(success){
                let companies = Companies.getFromData(json: data ?? Data())
                self.view?.confirmAPIResponse(companies: companies.enterprises)
            }

        }
    }
}
