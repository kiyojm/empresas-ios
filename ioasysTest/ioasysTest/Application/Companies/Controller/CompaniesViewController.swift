//
//  CompaniesViewController.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 11/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import UIKit

protocol CompaniesView: BaseView {
    func getCompanies()
    func confirmAPIResponse(companies: [Company])
}

class CompaniesViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var customSearchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerSizeConstraint: NSLayoutConstraint!
    @IBOutlet weak var companyCounterLabel: UILabel!
    var presenter: CompaniesPresenter = CompaniesPresenter()
    var isKeyboardActive:Bool = false
    var data: [Company] = []
    
    var filteredData: [Company] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        presenter.attach(view: self)
        presenter.getCompanies()
        loadingView.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func textFieldModified(_ sender: Any) {
        if(customSearchField.text != ""){
            filteredData = data.filter({
                $0.enterprise_name.range(of: customSearchField.text ?? "", options: .caseInsensitive) != nil
            })
            if(!isKeyboardActive){
                UIView.animate(withDuration: 0.5,
                               delay: 0.0,
                               options: [.curveEaseOut],
                               animations: {
                                self.headerSizeConstraint.constant -= 75
                                self.view.layoutIfNeeded()
                }, completion: nil)
            }
            companyCounterLabel.text = "\(filteredData.count) resultados encontrados"
            isKeyboardActive = true
            tableView.reloadData()
        }else{
            if(isKeyboardActive){
                UIView.animate(withDuration: 0.5,
                               delay: 0.0,
                               options: [.curveEaseOut],
                               animations: {
                                self.headerSizeConstraint.constant += 130
                                self.view.layoutIfNeeded()
                }, completion: nil)
            }
            isKeyboardActive = false
            filteredData = []
            companyCounterLabel.text = ""
            tableView.reloadData()
        }
        
        
    }
    
    @IBAction func textFieldDidEnd(_ sender: Any) {
        if(isKeyboardActive){
            UIView.animate(withDuration: 1.0,
                           delay: 0.0,
                           options: [.curveEaseOut],
                           animations: {
                            self.headerSizeConstraint.constant += 130
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
        isKeyboardActive = false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! CompanyTableViewCell
        cell.setCellData(companyName: filteredData[indexPath.row].enterprise_name,companyDescription: filteredData[indexPath.row].description)
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    func tableView(_ tableView: UITableView,
              didSelectRowAt indexPath: IndexPath){
        
        let currentCell = tableView.cellForRow(at: indexPath) as! CompanyTableViewCell
        
        performSegue(withIdentifier: "show_detail", sender: currentCell)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cell = sender as? CompanyTableViewCell
        if segue.identifier == "show_detail" {
            if let companyVC = segue.destination as? CompanyDetailViewController {
                companyVC.companyName = cell?.companyName.text
                companyVC.companyDescription = cell?.companyDescription
                companyVC.color = cell?.mainView.backgroundColor
                
            }
        }
    }
    //Não vá por esse caminho, jovem padawan
    /*
     func searchBar(_ searchBar?: UISearchBar, textDidChange searchText: String) {
     
     filteredData = searchText.isEmpty ? data : data.filter({(dataString: String) -> Bool in
     return dataString.range(of: searchText, options: .caseInsensitive) != nil
     })
     
     tableView.reloadData()
     }
     */
}

extension CompaniesViewController: CompaniesView {
    func getCompanies() {
        //nothing by now
    }
    
    func confirmAPIResponse(companies: [Company]) {
        loadingView.isHidden = true
        self.data = companies
        self.tableView.reloadData()
    }
}
