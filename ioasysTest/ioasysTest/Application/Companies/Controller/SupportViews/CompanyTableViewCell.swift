//
//  CompanyTableViewCell.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    var companyDescription:String?
    @IBOutlet weak var companyName: UILabel!
    
    func setCellData(companyName: String, companyDescription: String){
        self.companyName.text = companyName
        self.companyDescription = companyDescription
        mainView.backgroundColor = .random
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.companyName.text = ""
        self.mainView.backgroundColor = UIColor.white
    }
}
