//
//  CompanyDetailViewController.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import UIKit

class CompanyDetailViewController: UIViewController {

    
    @IBOutlet weak var viewHeader: UIView!
    var color:UIColor?
    var companyName: String?
    var companyDescription: String?
    @IBOutlet weak var companyNameLabel: UILabel?
    @IBOutlet weak var companyDescriptionText: UITextView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIColor.white.as1ptImage()
        self.title = companyName
        companyNameLabel?.text = companyName
        companyDescriptionText?.text = companyDescription
        viewHeader.backgroundColor = color
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
