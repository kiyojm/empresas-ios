//
//  LoginPresenter.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
class LoginPresenter: BasePresenter {
    
    weak private var view : LoginView?
    
    var service: AlamofireService = RestClient()
    
    func attach(view: NSObjectProtocol){
        self.view = view as? LoginView
    }
    
    func detach() {
        view = nil
    }
    
    
    func loginWith(email: String, password: String) {
        
        let params = ["email": email,
                      "password": password] as [String : Any]
        let path = SessionManager.LOGIN
        let url = path.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        service.POST(url: url, params, completionHandler: { (data, headers, success) -> Void in
            
            if(success){
                SessionManager.SetSession(token: headers?["access-token"] as! String, uid: headers?["uid"] as! String, client: headers?["client"] as! String)
                self.view?.confirmAPIResponse()
            }
            
            /*
            let urlT = SessionManager.COMPANIES
            self.service.GET(url: urlT) { (data, success) in
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any] {
                        // try to read out a string array
                        print(json)
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            */
        })
    }
}
