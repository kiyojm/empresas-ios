//
//  LoginViewController.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 10/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol LoginView: BaseView {
    @objc func requestLogin(email: String, password: String)
    @objc func confirmAPIResponse()
}

class LoginViewController: UIViewController {

    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var emailErrorField: UILabel!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordErrorField: UILabel!
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var welcomeTitle: UILabel!
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    var isKeyboardActive:Bool = false
    private let presenter: LoginPresenter = LoginPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        
        presenter.attach(view: self)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerContainer.round(corners: [.bottomLeft, .bottomRight], cornerRadius: 100)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    //testeapple@ioasys.com.br
    @IBAction func onClickLogin(_ sender: Any) {
        
        if(checkFields()){ //se retornar true, prosseguir
            requestLogin(email: emailField.text ?? "", password: passwordField.text ?? "")
            loadingView.isHidden = false
        }
        
    }
    func requestLogin(email: String, password: String) {
        presenter.loginWith(email: email, password: password)
        
    }
    @objc func keyboardWillAppear() {
        welcomeTitle.isHidden = true
        if(!isKeyboardActive){
            UIView.animate(withDuration: 1.0,
                               delay: 0.0,
                               options: [.curveEaseOut],
                               animations: {
                                 self.headerTopConstraint.constant -= 75
                                self.logoTopConstraint.constant += 50
                                 self.view.layoutIfNeeded()
                               }, completion: nil)
        }
        isKeyboardActive = true
    }

    @objc func keyboardWillDisappear() {
        welcomeTitle.isHidden = false
        if(isKeyboardActive){
            UIView.animate(withDuration: 1.0,
                           delay: 0.0,
                           options: [.curveEaseOut],
                           animations: {
                             self.headerTopConstraint.constant += 75
                            self.logoTopConstraint.constant -= 50
                             self.view.layoutIfNeeded()
                           }, completion: nil)
            isKeyboardActive = false
        }
    }
   
}

extension LoginViewController: LoginView {
    func confirmAPIResponse() {
        loadingView.isHidden = true
        let vc = UIStoryboard.buildCompaniesViewController()
        self.present(vc, animated: true, completion: nil)
    }
    func checkFields() -> Bool{
        if(passwordField.text == ""){
            passwordErrorField.isHidden = false
            passwordErrorField.text = "Este campo não pode ser vazio"
            return false
        }else if(emailField.text == ""){
            emailErrorField.isHidden = false
            emailErrorField.text = "Este campo não pode ser vazio"
            return false
        }
        return true
    }
    
}

extension UIView {
    func round(corners: UIRectCorner, cornerRadius: Double) {
        
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        
        //let path = UIBezierPath(arcCenter: CGPoint(x: 100, y: 100), radius: 100, startAngle: 0, endAngle: .pi, clockwise: false)
        let bezierPath = UIBezierPath(arcCenter: CGPoint(x:self.bounds.width/2, y: -60), radius: self.bounds.width/1.4, startAngle: 0, endAngle: .pi, clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds

        shapeLayer.path = bezierPath.cgPath
        self.layer.mask = shapeLayer
    }
}

