//
//  BaseView.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
@objc protocol BaseView: NSObjectProtocol {
    @objc optional func showAlert(title: String, message: String)
    @objc optional func loading(show: Bool)
}
