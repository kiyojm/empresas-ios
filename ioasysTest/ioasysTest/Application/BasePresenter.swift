//
//  BasePresenter.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
@objc protocol BasePresenter {
    @objc optional func attach(view: NSObjectProtocol)
    @objc optional func detach()
}
