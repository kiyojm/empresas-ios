//
//  AlamofireService.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
import Alamofire

protocol AlamofireService {
    
    func GET(url: String, headers: HTTPHeaders, completionHandler: @escaping (_ responseJSON: Data?, _ success: Bool) -> Void) -> Void
    
    func GET(url: String, completionHandler: @escaping (_ responseJSON: Data?, _ success: Bool) -> Void) -> Void
    
    func GET(url: String, completionHandler: @escaping (_ responseJSON: Data?, _ statusCode: Int?, _ success: Bool) -> Void) -> Void
    
    func POST(url: String, _ params: Parameters, completionHandler: @escaping (_ responseJSON: Data?, _ headers: [AnyHashable : Any]?, _ success: Bool) -> Void) -> Void
    
    func PUT(url: String, headers: HTTPHeaders, _ params: Parameters, completionHandler: @escaping (_ responseJSON: Data?, _ message: AnyObject?, _ success: Bool) -> Void) -> Void
    
    func PATCH(url: String, _ params: Parameters, completionHandler: @escaping (_ responseJSON: Data?, _ message: AnyObject?, _ success: Bool) -> Void) -> Void
    
    func DELETE(url: String, headers: HTTPHeaders, completionHandler: @escaping (_ responseJSON: Data?, _ message: AnyObject?, _ success: Bool) -> Void) -> Void
}
