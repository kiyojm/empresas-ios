//
//  SessionManager.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
import Alamofire

class SessionManager {
    static private var token:String = ""
    static private var uid:String = ""
    static private var client:String = ""
    
    private static var HOST: String = "https://empresas.ioasys.com.br/api/v1"
    
    //Auth
    static var LOGIN: String = "\(HOST)/users/auth/sign_in"
    
    //Feed
    static var COMPANIES: String = "\(HOST)/enterprises"
    
    struct APIManager {
        
        static var sessionManager: Alamofire.Session = {
            let configuration = URLSessionConfiguration.default
            var headers = AF.session.configuration.httpAdditionalHeaders
            headers?["Content-Type"] = "application/json"
            headers?["access-token"] = token
            headers?["uid"] = uid
            headers?["client"] = client
            
            configuration.httpAdditionalHeaders = headers
            return Alamofire.Session(
                configuration: configuration
            )
        }()
        
    }
    static func SetSession(token: String, uid: String, client: String){
        SessionManager.token = token
        SessionManager.uid = uid
        SessionManager.client = client
        APIManager.sessionManager = {
            let configuration = URLSessionConfiguration.default
            var headers = AF.session.configuration.httpAdditionalHeaders
            headers?["Content-Type"] = "application/json"
            headers?["access-token"] = token
            headers?["uid"] = uid
            headers?["client"] = client
            
            configuration.httpAdditionalHeaders = headers
            return Alamofire.Session(
                configuration: configuration
            )
        }()
    }
    public static func getSession()-> Alamofire.Session {
        return APIManager.sessionManager
    }
}
