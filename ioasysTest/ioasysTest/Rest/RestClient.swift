//
//  RestClient.swift
//  ioasysTest
//
//  Created by Joao Marcus Dionisio Araujo on 12/02/21.
//  Copyright © 2021 Joao Marcus Dionisio Araujo. All rights reserved.
//

import Foundation
import Alamofire

class RestClient: AlamofireService {
    
    func GET(url: String, headers: HTTPHeaders, completionHandler: @escaping (Data?, Bool) -> Void) {
        SessionManager.getSession().request(url, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, true)
            case .failure( _):
                completionHandler(response.data, false)
            }
        }
    }
    
    func GET(url: String, completionHandler: @escaping (Data?, Bool) -> Void) {
        SessionManager.getSession().request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, true)
            case .failure( _):
                completionHandler(response.data, false)
            }
        }
    }
    
    func GET(url: String, completionHandler: @escaping (Data?, Int? , Bool) -> Void) {
        SessionManager.getSession().request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, response.response?.statusCode,true)
            case .failure( _):
                completionHandler(response.data, response.response?.statusCode,false)
            }
        }
    }
    
    func POST(url: String, _ params: Parameters, completionHandler: @escaping (Data?, [AnyHashable : Any]?, Bool) -> Void) {
        SessionManager.getSession().request(url, method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, response.response?.allHeaderFields , true)
            case .failure( _):
                completionHandler(response.data, response.response?.allHeaderFields , false)
            }
        }
    }
    
    
    
    func PUT(url: String, headers: HTTPHeaders, _ params: Parameters, completionHandler: @escaping (Data?, AnyObject?, Bool) -> Void) {
        SessionManager.getSession().request(url, method: .put, parameters: params, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, "" as AnyObject, true)
            case .failure( _):
                completionHandler(response.data, response.error.debugDescription as AnyObject, false)
            }
        }
    }
    
    
    func PATCH(url: String, _ params: Parameters, completionHandler: @escaping (Data?, AnyObject?, Bool) -> Void) {
        SessionManager.getSession().request(url, method: .patch, parameters: params).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, "" as AnyObject, true)
            case .failure( _):
                completionHandler(response.data, response.error.debugDescription as AnyObject, false)
            }
        }
    }
    
    func DELETE(url: String, headers: HTTPHeaders, completionHandler: @escaping (Data?, AnyObject?, Bool) -> Void) {
        SessionManager.getSession().request(url, method: .delete, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                completionHandler(response.data, "" as AnyObject, true)
            case .failure( _):
                completionHandler(response.data, response.error.debugDescription as AnyObject, false)
            }
        }
    }
}
